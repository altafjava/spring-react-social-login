package com.altafjava.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
